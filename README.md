# docker-phppgadmin

Use https://gitlab.com/naturalis/lib/ansible-docker-compose to manage this docker-compose setup using Ansible. Or setup manually.

## Setup
- Copy env.dist to .env
- Modify .env file

## Start
```
docker-compose up -d
```
## Precommit gitleaks

This project has been protected by [gitleaks](https://github.com/gitleaks/gitleaks).
The pipeline is configured to scan on leaked secrets.

To be sure you do not push any secrets,
please [follow our guidelines](https://docs.aob.naturalis.io/standards/secrets/),
install [precommit](https://pre-commit.com/#install)
and run the commands:

 * `pre-commit autoupdate`
 * `pre-commit install`
